/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.junit.Test;

/**
 *
 * @author berk
 */
public class ExpressionParserTest {
    
    @Test
    public void parseTest() throws ElitException {
        ExpressionParser p = new ExpressionParser();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String result = sdf.format(new Date());
        org.junit.Assert.assertEquals("Parse date expression", result, p.parse("{$d}"));
    }
    
    @Test
    public void parseTest2() throws ElitException {
        ExpressionParser p = new ExpressionParser();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_YEAR, -2);
        String result = sdf.format(cal.getTime());
        org.junit.Assert.assertEquals("Parse date expression", result, p.parse("{$d(yyyyMMdd)-2}"));
    }
}
