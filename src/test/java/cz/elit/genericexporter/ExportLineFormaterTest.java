/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter;

import cz.elit.genericexporter.types.DummyValue;
import java.util.Calendar;
import org.junit.Test;

/**
 *
 * @author berk
 */
public class ExportLineFormaterTest {
    
    @Test
    public void getHeaderLineTest1() throws ElitException {
        String format[] = {"string","int","datetime(yyyy-MM-dd HH:mm:ss)"};
        String size[] = null;
        ExportLineFormater elf = new ExportLineFormater(format, size);
        elf.setDelimiter(";");
        
        DummyValue v;
        v = new DummyValue();
        v.setString("Name");
        elf.addHeaderField(v, 0);
        v = new DummyValue();
        v.setString("Age");
        elf.addHeaderField(v, 1);
        v = new DummyValue();
        v.setString("Some date");
        elf.addHeaderField(v, 2);
        
        org.junit.Assert.assertEquals("getHeaderLineTest1", "Name;Age;Some date", elf.getHeaderLine());
    }
    
    @Test
    public void getHeaderLineTest2() throws ElitException {
        String format[] = {"string","int","datetime(yyyy-MM-dd HH:mm:ss)"};
        String size[] = null;
        ExportLineFormater elf = new ExportLineFormater(format, size);
        elf.setDelimiter(";");
        
        DummyValue v;
        v = new DummyValue();
        v.setString("");
        elf.addHeaderField(v, 0);
        v = new DummyValue();
        v.setString("Age");
        elf.addHeaderField(v, 1);
        v = new DummyValue();
        v.setString("Some date");
        elf.addHeaderField(v, 2);
        
        org.junit.Assert.assertEquals("getHeaderLineTest1", ";Age;Some date", elf.getHeaderLine());
    }
    
    @Test
    public void getHeaderLineTest3() throws ElitException {
        String format[] = {"string","int","datetime(yyyy-MM-dd HH:mm:ss)"};
        String size[] = {"10","5","20"};
        ExportLineFormater elf = new ExportLineFormater(format, size);
        
        DummyValue v;
        v = new DummyValue();
        v.setString("Name");
        elf.addHeaderField(v, 0);
        v = new DummyValue();
        v.setString("Age");
        elf.addHeaderField(v, 1);
        v = new DummyValue();
        v.setString("Some date");
        elf.addHeaderField(v, 2);
        
        org.junit.Assert.assertEquals("getHeaderLineTest2", "Name      Age  Some date           ", elf.getHeaderLine());
    }
    
    @Test
    public void getLineTest1() throws ElitException {
        String format[] = {"string","int","datetime(yyyy-MM-dd HH:mm:ss)"};
        String size[] = null;
        ExportLineFormater elf = new ExportLineFormater(format, size);
        elf.setDelimiter(";");
        
        DummyValue v;
        v = new DummyValue();
        v.setString("Jarda");
        elf.addField(v, 0);
        v = new DummyValue();
        v.setInt(35);
        elf.addField(v, 1);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 2012);
        c.set(Calendar.MONTH, 01);
        c.set(Calendar.DAY_OF_MONTH, 22);
        c.set(Calendar.HOUR_OF_DAY, 13);
        c.set(Calendar.MINUTE, 14);
        c.set(Calendar.SECOND, 15);
        v = new DummyValue();
        v.setDateTime(c.getTime());
        elf.addField(v, 2);
        
        org.junit.Assert.assertEquals("getLineTest1", "Jarda;35;2012-02-22 13:14:15", elf.getLine());
    }
    
     @Test
    public void getLineTest2() throws ElitException {
        String format[] = {"string","int","datetime(yyyy-MM-dd HH:mm:ss)"};
        String size[] = null;
        ExportLineFormater elf = new ExportLineFormater(format, size);
        elf.setDelimiter(";");
        
        DummyValue v;
        v = new DummyValue();
        v.setString("");
        elf.addField(v, 0);
        v = new DummyValue();
        v.setInt(35);
        elf.addField(v, 1);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 2012);
        c.set(Calendar.MONTH, 01);
        c.set(Calendar.DAY_OF_MONTH, 22);
        c.set(Calendar.HOUR_OF_DAY, 13);
        c.set(Calendar.MINUTE, 14);
        c.set(Calendar.SECOND, 15);
        v = new DummyValue();
        v.setDateTime(c.getTime());
        elf.addField(v, 2);
        
        org.junit.Assert.assertEquals("getLineTest1", ";35;2012-02-22 13:14:15", elf.getLine());
    }
    
    @Test
    public void getLineTest3() throws ElitException {
        String format[] = {"string","int","datetime(yyyy-MM-dd HH:mm:ss)"};
        String size[] = {"10","5","20"};
        ExportLineFormater elf = new ExportLineFormater(format, size);
        
        DummyValue v;
        v = new DummyValue();
        v.setString("Jarda");
        elf.addField(v, 0);
        v = new DummyValue();
        v.setInt(35);
        elf.addField(v, 1);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, 2012);
        c.set(Calendar.MONTH, 01);
        c.set(Calendar.DAY_OF_MONTH, 22);
        c.set(Calendar.HOUR_OF_DAY, 13);
        c.set(Calendar.MINUTE, 14);
        c.set(Calendar.SECOND, 15);
        v = new DummyValue();
        v.setDateTime(c.getTime());
        elf.addField(v, 2);
        
        org.junit.Assert.assertEquals("getLineTest2", "Jarda        352012-02-22 13:14:15 ", elf.getLine());
    }
}
