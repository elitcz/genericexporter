/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import org.junit.Test;

/**
 *
 * @author berk
 */
public class NullTypeTest {
    
    @Test
    public void toStringTest1() {
        NullType t = new NullType();
        org.junit.Assert.assertEquals("NullType1: toString test", "null", t.toString());
    }
    
    @Test
    public void toStringTest2() {
        NullType t = new NullType();
        t.setFixedSize(10);
        org.junit.Assert.assertEquals("StringType2: toString test", "null      ", t.toString());
    }
}
