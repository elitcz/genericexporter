/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import java.util.Calendar;
import org.junit.Test;

/**
 *
 * @author berk
 */
public class DateTimeTypeTest {
    
    @Test
    public void toStringTest1() {
        DateTimeType t = new DateTimeType();
        t.setFormat("yyyy-MM-dd HH:mm:ss");
        
        Calendar c = Calendar.getInstance();
        
        c.set(Calendar.YEAR, 2012);
        c.set(Calendar.MONTH, 01);
        c.set(Calendar.DAY_OF_MONTH, 22);
        c.set(Calendar.HOUR_OF_DAY, 13);
        c.set(Calendar.MINUTE, 14);
        c.set(Calendar.SECOND, 15);
        
        DummyValue v = new DummyValue();
        v.setDateTime(c.getTime());
        t.setValue(v);
        org.junit.Assert.assertEquals("DateTimeType1: toString test", "2012-02-22 13:14:15", t.toString());
    }
    
    @Test
    public void toStringTest2() {
        DateTimeType t = new DateTimeType();
        t.setFormat("yyyy-MM-dd HH:mm:ss");
        t.setFixedSize(25);
        
        Calendar c = Calendar.getInstance();
        
        c.set(Calendar.YEAR, 2012);
        c.set(Calendar.MONTH, 01);
        c.set(Calendar.DAY_OF_MONTH, 22);
        c.set(Calendar.HOUR_OF_DAY, 13);
        c.set(Calendar.MINUTE, 14);
        c.set(Calendar.SECOND, 15);
        
        DummyValue v = new DummyValue();
        v.setDateTime(c.getTime());
        t.setValue(v);
        org.junit.Assert.assertEquals("DateTimeType2: toString test", "2012-02-22 13:14:15      ", t.toString());
    }
    
    @Test
    public void toStringTest3() {
        DateTimeType t = new DateTimeType();
        t.setFormat("yyyy-MM-dd HH:mm:ss");
        DummyValue v = new DummyValue();
        v.setDateTime(null);
        t.setValue(v);
        org.junit.Assert.assertEquals("DateTimeType3: toString test", "null", t.toString());
    }
}
