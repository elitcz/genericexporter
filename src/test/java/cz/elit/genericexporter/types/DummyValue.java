/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import java.util.Date;

/**
 *
 * @author berk
 */
public class DummyValue implements IValue {
    
    private Date dateValue;
    private double doubleValue;
    private int intValue;
    private String stringValue;
    
    public Date getDateTime() {
        return dateValue;
    }

    public Date getDate() {
        return dateValue;
    }

    public double getDouble() {
        return doubleValue;
    }

    public int getInt() {
        return intValue;
    }

    public String getString() {
        return stringValue;
    }
    
    public void setDateTime(Date dateValue) {
        this.dateValue = dateValue;
    }
    
    public void setDate(Date dateValue) {
        this.dateValue = dateValue;
    }
    
    public void setDouble(double doubleValue) {
        this.doubleValue = doubleValue;
    }
    
    public void setInt(int intValue) {
        this.intValue = intValue;
    }
    
    public void setString(String stringValue) {
        this.stringValue = stringValue;
    }
}
