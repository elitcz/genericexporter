/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import org.junit.Test;

/**
 *
 * @author berk
 */
public class StringTypeTest {

    @Test
    public void toStringTest1() {
        StringType t = new StringType();
        DummyValue v = new DummyValue();
        v.setString("My test");
        t.setValue(v);
        org.junit.Assert.assertEquals("StringType1: toString test", "My test", t.toString());
    }
    
    @Test
    public void toStringTest2() {
        StringType t = new StringType();
        t.setFixedSize(10);
        DummyValue v = new DummyValue();
        v.setString("My test");
        t.setValue(v);
        org.junit.Assert.assertEquals("StringType2: toString test", "My test   ", t.toString());
    }
    
    @Test
    public void toStringTest3() {
        StringType t = new StringType();
        DummyValue v = new DummyValue();
        v.setString(null);
        t.setValue(v);
        org.junit.Assert.assertEquals("StringType3: toString test", "null", t.toString());
    }
}
