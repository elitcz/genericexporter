/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import org.junit.Test;

/**
 *
 * @author berk
 */
public class IntTypeTest {
            
    @Test
    public void toStringTest1() {
        IntType t = new IntType();
        DummyValue v = new DummyValue();
        v.setInt(10);
        t.setValue(v);
        org.junit.Assert.assertEquals("IntType1: toString test", "10", t.toString());
    }
    
    @Test
    public void toStringTest2() {
        IntType t = new IntType();
        t.setFixedSize(10);
        DummyValue v = new DummyValue();
        v.setInt(10);
        t.setValue(v);
        org.junit.Assert.assertEquals("IntType2: toString test", "        10", t.toString());
    }
}
