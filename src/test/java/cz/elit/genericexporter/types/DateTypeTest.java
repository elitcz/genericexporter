/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import java.util.Calendar;
import org.junit.Test;

/**
 *
 * @author berk
 */
public class DateTypeTest {
    
    @Test
    public void toStringTest1() {
        DateType t = new DateType();
        t.setFormat("yyyy-MM-dd");
        
        Calendar c = Calendar.getInstance();
        
        c.set(Calendar.YEAR, 2012);
        c.set(Calendar.MONTH, 01);
        c.set(Calendar.DAY_OF_MONTH, 22);
        c.set(Calendar.HOUR_OF_DAY, 13);
        c.set(Calendar.MINUTE, 14);
        c.set(Calendar.SECOND, 15);
        
        DummyValue v = new DummyValue();
        v.setDate(c.getTime());
        t.setValue(v);
        org.junit.Assert.assertEquals("DateType1: toString test", "2012-02-22", t.toString());
    }
    
    @Test
    public void toStringTest2() {
        DateType t = new DateType();
        t.setFormat("yyyy-MM-dd");
        t.setFixedSize(25);
        
        Calendar c = Calendar.getInstance();
        
        c.set(Calendar.YEAR, 2012);
        c.set(Calendar.MONTH, 01);
        c.set(Calendar.DAY_OF_MONTH, 22);
        c.set(Calendar.HOUR_OF_DAY, 13);
        c.set(Calendar.MINUTE, 14);
        c.set(Calendar.SECOND, 15);
        
        DummyValue v = new DummyValue();
        v.setDate(c.getTime());
        t.setValue(v);
        org.junit.Assert.assertEquals("DateType2: toString test", "2012-02-22               ", t.toString());
    }
    
    @Test
    public void toStringTest3() {
        DateType t = new DateType();
        t.setFormat("yyyy-MM-dd");
        DummyValue v = new DummyValue();
        v.setDate(null);
        t.setValue(v);
        org.junit.Assert.assertEquals("DateType3: toString test", "null", t.toString());
    }
}
