/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import java.util.Calendar;
import org.junit.Test;

/**
 *
 * @author berk
 */
public class DoubleTypeTest {
        
    @Test
    public void toStringTest1() {
        DoubleType t = new DoubleType();
        t.setFormat("##0.00");
        DummyValue v = new DummyValue();
        v.setDouble(3.14159d);
        t.setValue(v);
        org.junit.Assert.assertEquals("DoubleType1: toString test", "3.14", t.toString());
    }
    
    @Test
    public void toStringTest2() {
        DoubleType t = new DoubleType();
        t.setFormat("##0.00");
        t.setFixedSize(10);
        DummyValue v = new DummyValue();
        v.setDouble(3.14159d);
        t.setValue(v);
        org.junit.Assert.assertEquals("DoubleType2: toString test", "      3.14", t.toString());
    }
}
