
package cz.elit.genericexporter.config;

import java.io.File;

public class ConfigFactory {

    public static IConfig get(File file) throws Exception {
        if (file.getAbsolutePath().endsWith(".properties")) {
            return new PropsConfig(file);
        }
        return new ConfConfig(file);
    }
}
