
package cz.elit.genericexporter.config;

public interface IConfig {

    public String jobName();
    public String jobCommand();
    public String logFile();
    public String filename();
    public String fileArchive();

    public String dbDriver();
    public String dbConnString();
    public String dbLogin();
    public String dbPwd();

    public String sqlIn();
    
    public String sqlOutInsert();
    public String sqlOutDelete();

    public String ftpHost();
    public String ftpUser();
    public String ftpPwd();
    public String ftpPort();
    public String ftpDir();
    
    public String ftpsHost();
    public String ftpsUser();
    public String ftpsPwd();
    public String ftpsPort();
    public String ftpsDir();

    public String sftpHost();
    public String sftpUser();
    public String sftpPwd();
    public String sftpPort();
    public String sftpDir();

    public String csvDelimiter();
    public String[] csvFormat();
    public boolean csvUseHeader();
    public String csvEncoding();
    public String[] csvFixedSize();

    public String outDbDriver();
    public String outDbConnString();
    public String outDbLogin();
    public String outDbPwd();
    
    public String eMailSmtp();
    public String eMailLogin();
    public String eMailPassword();
    public String eMailSender();
    public String eMailRecipients();
    public boolean eMailNotifyOnSuccess();
    public boolean eMailNotifyOnFailure();
    public boolean eMailSendReport();
    public String eMailReportSubject();
    public String eMailReportMessage();
    
}
