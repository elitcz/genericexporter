package cz.elit.genericexporter.config;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;


public class PropsConfig implements IConfig {
    
    private PropertiesConfiguration props =  new PropertiesConfiguration();

    public PropsConfig(File f) throws ConfigurationException {
        props.load(f);
    }

    private String sql(String key) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(props.getString(key)));

        String out = "";
        String line;
        while ((line = reader.readLine()) != null) {
            out += "\n" + line;
        }
        reader.close();
        return out;
    }
    
    public String jobName() {
        return props.getString("job.name");
    }
    
    public String jobCommand() {
        return props.getString("job.command");
    }

    public String dbDriver() {
        return props.getString("db.driver");
    }

    public String dbLogin() {
        return props.getString("db.login");
    }

    public String dbPwd() {
        return props.getString("db.pwd");
    }

    public String filename() {
        return props.getString("file.name");
    }

    public String sqlIn() {
        try {
            return this.sql("sql.in");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PropsConfig.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        } catch (IOException ex) {
            Logger.getLogger(PropsConfig.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    public String sqlOutInsert() {
        return props.getString("sql.out.insert");
    }

    public String logFile() {
        return props.getString("log.file");
    }

    public String fileArchive() {
        return props.getString("file.archive");
    }

    public String dbConnString() {
        return props.getString("db.connection.string");
    }

    public String sqlOutDelete() {
        return props.getString("sql.out.delete");
    }

    public String ftpHost() {
        return props.getString("ftp.host");
    }

    public String ftpUser() {
        return props.getString("ftp.user");
    }

    public String ftpPwd() {
        return props.getString("ftp.password");
    }
    
    public String ftpPort() {
        return props.getString("ftp.port");
    }

    public String ftpDir() {
        return props.getString("ftp.directory");
    }
    
    public String ftpsHost() {
        return props.getString("ftps.host");
    }

    public String ftpsUser() {
        return props.getString("ftps.user");
    }

    public String ftpsPwd() {
        return props.getString("ftps.password");
    }
    
    public String ftpsPort() {
        return props.getString("ftps.port");
    }

    public String ftpsDir() {
        return props.getString("ftps.directory");
    }

    public String sftpHost() {
        return props.getString("sftp.host");
    }

    public String sftpUser() {
        return props.getString("sftp.user");
    }

    public String sftpPwd() {
        return props.getString("sftp.password");
    }

    public String sftpPort() {
        return props.getString("sftp.port");
    }
    
    public String sftpDir() {
        return props.getString("sftp.directory");
    }

    public String csvDelimiter() {
        return props.getString("csv.delimiter");
    }

    public String[] csvFormat() {
        return props.getStringArray("csv.cols.format");
    }

    public boolean csvUseHeader() {
        return props.getBoolean("csv.use.header", false);
    }

    public String csvEncoding() {
        return props.getString("csv.encoding");
    }

    public String[] csvFixedSize() {
        return props.getStringArray("csv.fixed.size");
    }

    public String outDbDriver() {
        return props.getString("out.db.driver");
    }

    public String outDbConnString() {
        return props.getString("out.db.connection.string");
    }

    public String outDbLogin() {
        return props.getString("out.db.login");
    }

    public String outDbPwd() {
        return props.getString("out.db.password");
    }

    public String eMailSmtp() {
        return props.getString("email.smtp");
    }

    public String eMailLogin() {
        return props.getString("email.login");
    }

    public String eMailPassword() {
        return props.getString("email.password");
    }

    public String eMailSender() {
        return props.getString("email.sender");
    }

    public String eMailRecipients() {
        return props.getString("email.recipients");
    }

    public boolean eMailNotifyOnSuccess() {
        return props.getBoolean("email.notifyonsuccess", false);
    }

    public boolean eMailNotifyOnFailure() {
        return props.getBoolean("email.notifyonfailure", false);
    }
    
    public boolean eMailSendReport() {
        return props.getBoolean("email.sendreport", false);
    }
    public String eMailReportSubject() {
        return props.getString("email.reportsubject");
    }
    
    public String eMailReportMessage() {
        return props.getString("email.reportmessage");
    }
}
