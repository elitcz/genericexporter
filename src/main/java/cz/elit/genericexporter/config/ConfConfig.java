
package cz.elit.genericexporter.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeMap;

public class ConfConfig implements IConfig {

    private File file;
    private TreeMap<String, String> config = new TreeMap<String, String>();

    public ConfConfig(File f) throws IOException {
        this.file = f;
        parse(file);
    }
    
    private String get(String key) {
        return config.get(key);
    }

    private void parse(File f) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(f));

        String line;
        while ((line = reader.readLine()) != null) {
            if (containsData(line)) {
                int i = line.indexOf(":");
                String key = line.substring(0, i).trim();
                String value = line.substring(i + 1).trim();
                //-- multiline entry
                if (value.equals("")) {
                    while ((line = reader.readLine()) != null) {
                        if (line.trim().equals("")) {
                            break;
                        }
                        value = value + line + "\n";
                    }
                }
                if (key.equals("$include")) {
                    File inclFile = new File(value);
                    if (inclFile.canRead()) {
                        parse(inclFile);
                    } else {
                        throw new RuntimeException("Cannot read included file " + inclFile);
                    }
                } else {
                    config.put(key, value);
                }
            }
        }
        reader.close();
    }

    private boolean containsData(String line) {
        boolean result = true;
        if (line.startsWith("#")) {
            result = false;
        }
        if (!line.contains(":")) {
            result = false;
        }
        return result;
    }
    
    public String jobName() {
        return this.get("Job Name");
    }
    
    public String jobCommand() {
        return this.get("Job Command");
    }

    public String logFile() {
        return this.get("Log File");
    }

    public String filename() {
        return this.get("File Name");
    }

    public String fileArchive() {
        return this.get("Archive Name");
    }

    public String dbDriver() {
        return this.get("Database Driver");
    }

    public String dbConnString() {
        return this.get("Connection String");
    }

    public String dbLogin() {
        return this.get("Login");
    }

    public String dbPwd() {
        return this.get("Password");
    }

    public String sqlIn() {
        return this.get("SQL");
    }

    public String sqlOutInsert() {
        return this.get("Dest. SQL_INSERT");
    }

    public String sqlOutDelete() {
        return this.get("Dest. SQL_DELETE");
    }

    public String ftpHost() {
        return this.get("FTP host");
    }

    public String ftpUser() {
        return this.get("FTP user");
    }

    public String ftpPwd() {
        return this.get("FTP password");
    }
    
    public String ftpPort() {
        return this.get("FTP port");
    }

    public String ftpDir() {
        return this.get("FTP directory");
    }
    
    public String ftpsHost() {
        return this.get("FTPS host");
    }

    public String ftpsUser() {
        return this.get("FTPS user");
    }

    public String ftpsPwd() {
        return this.get("FTPS password");
    }
    
    public String ftpsPort() {
        return this.get("FTPS port");
    }

    public String ftpsDir() {
        return this.get("FTPS directory");
    }

    public String sftpHost() {
        return this.get("SFTP host");
    }

    public String sftpUser() {
        return this.get("SFTP user");
    }

    public String sftpPwd() {
        return this.get("SFTP password");
    }

    public String sftpPort() {
        return this.get("SFTP port");
    }
    
    public String sftpDir() {
        return this.get("SFTP directory");
    }

    public String csvDelimiter() {
        return this.get("Delimiter");
    }

    public String[] csvFormat() {
        String result[] = null;
        if (this.get("Format") != null) {
            result = this.get("Format").split(",");
        }
        return result;
    }

    public boolean csvUseHeader() {
        boolean result = false;
        if ((this.get("Print header") != null) && (!this.get("Print header").equals("0"))) {
            result = true;
        }
        return result;
    }

    public String csvEncoding() {
        return this.get("Encoding");
    }

    public String[] csvFixedSize() {
        String result[] = null;
        if (this.get("Fixed size") != null) {
            result = this.get("Fixed size").split(",");
        }
        return result;
    }

    public String outDbDriver() {
        return this.get("Dest. Database Driver");
    }

    public String outDbConnString() {
        return this.get("Dest. Connection String");
    }

    public String outDbLogin() {
        return this.get("Dest. Login");
    }

    public String outDbPwd() {
        return this.get("Dest. Password");
    }

    public String eMailSmtp() {
        return this.get("eMail Smtp");
    }

    public String eMailLogin() {
        return this.get("eMail Login");
    }

    public String eMailPassword() {
        return this.get("eMail Password");
    }

    public String eMailSender() {
        return this.get("eMail Sender");
    }

    public String eMailRecipients() {
        return this.get("eMail Recipients");
    }

    public boolean eMailNotifyOnSuccess() {
        String value = this.get("eMail Notify On Success");
        return ((value != null) && (value.equals("1"))) ? true : false;
    }

    public boolean eMailNotifyOnFailure() {
        String value = this.get("eMail Notify On Failure");
        return ((value != null) && (value.equals("1"))) ? true : false;
    }
    
    public boolean eMailSendReport() {
        String value = this.get("eMail Send Report");
        return ((value != null) && (value.equals("1"))) ? true : false;
    }
    public String eMailReportSubject() {
        return this.get("eMail Report Subject");
    }
    
    public String eMailReportMessage() {
        return this.get("eMail Report Message");
    }
}
