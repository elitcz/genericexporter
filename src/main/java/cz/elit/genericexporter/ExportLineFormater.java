
package cz.elit.genericexporter;

import cz.elit.genericexporter.types.*;
import java.util.ArrayList;

public final class ExportLineFormater extends ImportExportLineProcessor {

    private String delimiter = null;
    private ArrayList<AbstractType> header;
    private ArrayList<AbstractType> record;
    private String line;
    private String headerLine;

    public ExportLineFormater(String[] format, String[] fixedSize) {
        clear();
        record = createRecord(format, fixedSize);
        header = createHeaderRecord(format, fixedSize);
    }

    public void setDelimiter(String delimiter) throws ElitException {
        this.delimiter = new ExpressionParser().parse(delimiter);
    }
    
    public void clear() {
        line = "";
        headerLine = "";
    }

    public void addField(IValue v, int index) {
        record.get(index).setValue(v);
        if ((index != 0) && (delimiter != null)) {
            line += delimiter;
        }
        line += record.get(index).toString();
    }
    
    public void addHeaderField(IValue v, int index) {
        header.get(index).setValue(v);
        if ((index != 0) && (delimiter != null)) {
            headerLine += delimiter;
        }
        headerLine += header.get(index).toString();
    }

    public String getLine() {
        return line;
    }
    
    public String getHeaderLine() {
        return headerLine;
    }
}
