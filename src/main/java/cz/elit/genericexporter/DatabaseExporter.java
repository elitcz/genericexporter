
package cz.elit.genericexporter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseExporter {

    private Connection srcCnn;
    private Connection destCnn;
    private String srcQuery;
    private String destDelete;
    private String destInsert;

    public DatabaseExporter(Connection srcCnn, String srcQuery, Connection destCnn, String destDelete, String destInsert) throws ElitException {
        this.srcCnn = srcCnn;
        this.destCnn = destCnn;
        
        ExpressionParser p = new ExpressionParser();
        this.srcQuery = p.parse(srcQuery);

        ExpressionParser pd = new ExpressionParser();
        this.destDelete = pd.parse(destDelete);

        ExpressionParser pi = new ExpressionParser();
        this.destInsert = pi.parse(destInsert);
    }

    public void run() throws SQLException {
        destCnn.setAutoCommit(false);
        if ((destDelete != null) && (!destDelete.equals(""))) {
            Statement st = destCnn.createStatement();
            st.execute(destDelete);
            st.close();
        }
        PreparedStatement ps = destCnn.prepareStatement(destInsert);
        Statement st = srcCnn.createStatement();
        ResultSet rs = st.executeQuery(srcQuery);
        ResultSetMetaData rsmd = rs.getMetaData();
        int n = rsmd.getColumnCount();
        while(rs.next()) {
            for (int i = 0; i < n; i++) {
                int type = rsmd.getColumnType(i + 1);
                switch (type) {
                    case java.sql.Types.DECIMAL :
                        ps.setDouble(i + 1, rs.getDouble(i + 1));
                        break;
                    case java.sql.Types.BLOB :
                        ps.setBytes(i + 1, rs.getBytes(i + 1));
                        break;
                    case java.sql.Types.BINARY :
                        ps.setBytes(i + 1, rs.getBytes(i + 1));
                        break;
                    case java.sql.Types.LONGVARBINARY :
                        ps.setBytes(i + 1, rs.getBytes(i + 1));
                        break;
                    default :
                        ps.setString(i + 1, rs.getString(i + 1));
                }
            }
            ps.execute();
        }
        ps.close();
        rs.close();
        st.close();
        destCnn.commit();
    }
}
