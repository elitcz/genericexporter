
package cz.elit.genericexporter;

import com.jcraft.jsch.*;

public class SftpClient {

    private String host;
    private int port;
    private String user;
    private String password;
    private String remoteDir;

    public SftpClient(String host, String remoteDir, int port, String user, String password) {
        this.host = host;
        this.remoteDir = remoteDir;
        this.port = port;
        this.user = user;
        this.password = password;
    }

    public void put(String file, String name) throws JSchException, SftpException {
        JSch jsch = new JSch();
        Session session = jsch.getSession(user, host, port);

        MyUserInfo ui = new MyUserInfo();
        ui.setPassword(password);
        session.setUserInfo(ui);
        session.connect();
        Channel channel = session.openChannel("sftp");
        channel.connect();
        ChannelSftp c = (ChannelSftp) channel;
        
        c.put(file, remoteDir + "/" + name, ChannelSftp.OVERWRITE);
        ls(c);

        session.disconnect();
    }

    private void ls(ChannelSftp c) throws SftpException {
        java.util.Vector vv = c.ls(".");
        if (vv != null) {
            for (int ii = 0; ii < vv.size(); ii++) {
                Object obj = vv.elementAt(ii);
                if (obj instanceof com.jcraft.jsch.ChannelSftp.LsEntry) {
                    System.out.println(((com.jcraft.jsch.ChannelSftp.LsEntry) obj).getLongname());
                }
            }
        }
    }

    public static class MyUserInfo implements UserInfo {
        private String passwd;

        public String getPassphrase() {
            return null;
        }

        public void setPassword(String p) {
            passwd = p;
        }

        public String getPassword() {
            return passwd;
        }

        public boolean promptPassword(String string) {
            return true;
        }

        public boolean promptPassphrase(String string) {
            return true;
        }

        public boolean promptYesNo(String string) {
            return true;
        }

        public void showMessage(String string) {
        }
    }
}
