/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter;

import cz.elit.genericexporter.types.AbstractType;
import java.util.ArrayList;

/**
 *
 * @author berk
 */
public class ImportLineParser extends ImportExportLineProcessor {

    private String delimiter = null;
    private String line;
    private ArrayList<AbstractType> record;
    
    public ImportLineParser(String[] format, String[] fixedSize) {
        clear();
        record = createRecord(format, fixedSize);
    }
    
    public void clear() {
        setLine("");
    }
    
    private void parse() {
        if (delimiter != null) {
            String fields[] = line.split(delimiter);
            for (int i = 0; i < record.size(); i++) {
                AbstractType t = record.get(i);
                t.setValue(fields[i]);
            }
        }
    }

    /**
     * @return the delimiter
     */
    public String getDelimiter() {
        return delimiter;
    }

    /**
     * @param delimiter the delimiter to set
     */
    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }
    
    public int getNumberOfColumns() {
        return record.size();
    }
    
    public AbstractType getField(int i) {
        return record.get(i);
    }

    /**
     * @return the line
     */
    public String getLine() {
        return line;
    }

    /**
     * @param line the line to set
     */
    public void setLine(String line) {
        this.line = line;
        parse();
    }
}
