/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author berk
 */
public class FileImporter {
    
    private Connection cnn;
    private String insert;
    private File fin;
    private String encoding;
    private ImportLineParser importLineParser;
    private boolean skipHeader;
    
    public FileImporter(Connection cnn, String insert, ImportLineParser ilp, File fin, String encoding, boolean skipHeader) throws ElitException {
        this.cnn = cnn;
        ExpressionParser p = new ExpressionParser();
        this.insert = p.parse(insert);
        this.importLineParser = ilp;
        this.fin = fin;
        this.encoding = encoding;
        this.skipHeader = skipHeader;
    }
    
    public void run() throws IOException, SQLException {
        FileInputStream in = new FileInputStream(fin);
        BufferedReader reader;
        if (encoding != null) {
            reader = new BufferedReader(new InputStreamReader(in, encoding));
        } else {
            reader = new BufferedReader(new InputStreamReader(in));
        }
        String line;
        int lineNo = 0;
        PreparedStatement ps = cnn.prepareStatement(insert);
        while ((line = reader.readLine()) != null) {
            lineNo++;
            if (skipHeader && (lineNo == 1)) {
                continue;
            }
            importLineParser.setLine(line);
            for (int i = 0; i < importLineParser.getNumberOfColumns(); i++) {
                ps.setString(i + 1, importLineParser.getField(i).toString());
            }
            ps.execute();
        }
        ps.close();
        reader.close();
    }
}