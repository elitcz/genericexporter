/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.jar.Manifest;

/**
 *
 * @author berk
 */
public class Version {
    
    private Manifest mf = null;
    
    public Version() {
        URLClassLoader cl = (URLClassLoader) getClass().getClassLoader();
        try {
            URL url = cl.findResource("META-INF/MANIFEST.MF");
            mf = new Manifest(url.openStream());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getVersion() {
        String result = "";
        if (mf != null) {
            String name =   mf.getMainAttributes().getValue("Implementation-Title") + " " +
                            mf.getMainAttributes().getValue("Implementation-Version") + "\n";
            String vendor = "(c) 2010 " + mf.getMainAttributes().getValue("Implementation-Vendor") + "\n";
            String url =    mf.getMainAttributes().getValue("url") + "\n";
            int i = 0;
            if (name.length() > i) i = name.length();
            if (vendor.length() > i) i = vendor.length();
            if (url.length() > i) i = url.length();
            String hr = "";
            for (int n = 0; n < i; n++) {
                hr += "-";
            }
            hr += "\n";
            result = hr + name + vendor + url + hr;
        }
        return result;
    }
    
    public String getHelp() {
        String result = "Usage: java -jar GenericExporter configFile\n";
        return result;
    }
}
