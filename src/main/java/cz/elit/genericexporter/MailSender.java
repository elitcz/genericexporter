/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter;

import java.io.File;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;

/**
 *
 * @author berk
 */
public class MailSender {

    private String smtp;
    private String login;
    private String password;
    private String sender;

    public MailSender(String smtp, String login, String password, String sender) {
        this.smtp = smtp;
        this.login = login;
        this.password = password;
        this.sender = sender;
    }

    public void sendMail(String recipient, String subject, String body) throws EmailException {
        HtmlEmail email = new HtmlEmail();
        email.setCharset("UTF-8");
        email.setHostName(smtp);
        if ((login != null) && (!login.equals(""))) {
            email.setAuthentication(login, password);
        }
        String r[] = recipient.split(",");
        for (int i = 0; i < r.length; i++) {
            email.addTo(r[i], r[i]);
        }
        email.setFrom(sender, sender);
        email.setSubject(subject);
        // embed the image and get the content id
        //URL url = new URL("http://www.apache.org/images/asf_logo_wide.gif");
        //String cid = email.embed(url, "Apache logo");
        // set the html message
        //email.setHtmlMsg("<html>The apache logo - <img src=\"cid:"+cid+"\"></html>");
        // set the alternative message
        //email.setTextMsg("Your email client does not support HTML messages");
        email.setHtmlMsg(body);
        email.send();
    }
    
    public void sendMail(String recipient, String subject, String body, File f[]) throws EmailException {
        
        // Create the email message
        MultiPartEmail email = new MultiPartEmail();
        email.setCharset("UTF-8");
        email.setHostName(smtp);
        if ((login != null) && (!login.equals(""))) {
            email.setAuthentication(login, password);
        }
        String r[] = recipient.split(",");
        for (int i = 0; i < r.length; i++) {
            email.addTo(r[i], r[i]);
        }
        email.setFrom(sender, sender);
        email.setSubject(subject);
        email.setMsg(body);
        
        if (f != null) {
            for (int i = 0; i < f.length; i++) {
                // Create the attachment
                EmailAttachment attachment = new EmailAttachment();
                attachment.setPath(f[i].getAbsolutePath());
                attachment.setDisposition(EmailAttachment.ATTACHMENT);
                attachment.setDescription(f[i].getName());
                attachment.setName(f[i].getName());
                email.attach(attachment);
            }
        }
        
        email.send();
    }
}
