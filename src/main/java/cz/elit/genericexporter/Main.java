
package cz.elit.genericexporter;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import cz.elit.genericexporter.config.ConfigFactory;
import cz.elit.genericexporter.config.IConfig;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.*;
import org.apache.commons.mail.EmailException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPSClient;

public class Main {

    static Logger l = Logger.getLogger("GenericExporterLog");
    private static IConfig conf;

    public static void main(String[] args) {
        Version v = new Version();
        System.out.println(v.getVersion());
        
        if (args.length != 1) {
            System.out.println(v.getHelp());
            return;
        }

        try {
            conf = ConfigFactory.get(new File(args[0]));
            initLogger();
            l.info("Running job " + conf.jobName());
            
            if (conf.jobCommand() == null || conf.jobCommand().equals("")) {
                throw new ElitException("Job command is not set");
            }
                
            if (conf.jobCommand().equals("exportToFile")) {
                exportToFile();
            }
            
            if (conf.jobCommand().equals("exportToDatabase")) {
                exportToDatabase();
            }
            
            if (conf.jobCommand().equals("importFromFile")) {
                importToDatabase();
            }
            notifySuccess();
            
        } catch (Exception ex) {
            l.log(Level.SEVERE, null, ex);
            notifyFailure(ex);
            throw new RuntimeException(ex.getMessage());
        }

    }

    private static void initLogger() throws IOException {
        if (conf.logFile() != null) {
            Handler handler = new FileHandler(conf.logFile(), true);
            handler.setFormatter(new SimpleFormatter());
            l.addHandler(handler);
        }
        
        //if (conf.localPortalWsdl() != null && !conf.localPortalWsdl().isEmpty()) {
        //    l.addHandler(new ProcessLogHandler(conf.localPortalTask(), conf.localPortalWsdl()));
        //}
    }

    private static void exportToDatabase() throws Exception {
        Connection srcCnn = null;
        Connection destCnn = null;

        try {
            Class.forName(conf.dbDriver());
            if (conf.dbLogin() != null) {
                srcCnn = java.sql.DriverManager.getConnection(conf.dbConnString(), conf.dbLogin(), conf.dbPwd());
            } else {
                srcCnn = java.sql.DriverManager.getConnection(conf.dbConnString());
            }

            srcCnn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);

            Class.forName(conf.outDbDriver());
            if (conf.outDbLogin() != null) {
                destCnn = java.sql.DriverManager.getConnection(conf.outDbConnString(), conf.outDbLogin(), conf.outDbPwd());
            } else {
                destCnn = java.sql.DriverManager.getConnection(conf.outDbConnString());
            }

            DatabaseExporter e = new DatabaseExporter(
                srcCnn,
                conf.sqlIn(),
                destCnn,
                conf.sqlOutDelete(),
                conf.sqlOutInsert()
            );
            l.info("Exporting data...");
            e.run();
            l.info("Done!");
        } finally {
            if (srcCnn != null) {
                srcCnn.close();
            }
            if (destCnn != null) {
                destCnn.close();
            }
        }
    }

    private static void exportToFile() throws Exception {
        Connection cnn = null;

        try {
            l.info("Connectiong to DB...");
            Class.forName(conf.dbDriver());
            if (conf.dbLogin() != null) {
                cnn = java.sql.DriverManager.getConnection(conf.dbConnString(), conf.dbLogin(), conf.dbPwd());
            } else {
                cnn = java.sql.DriverManager.getConnection(conf.dbConnString());
            }

            cnn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);

            l.info("Preparing output file...");
            File f = new File(new ExpressionParser().parse(conf.filename()));
            exportData(f, cnn);

            if (conf.fileArchive() != null) {
                makeZip(f);
                f = new File(new ExpressionParser().parse(conf.fileArchive()));
            }
            
            if (conf.ftpHost() != null) {
                exportToFtp(f);
            }
            
            if (conf.ftpsHost() != null) {
                exportToFtps(f);
            }

            if (conf.sftpHost() != null) {
                exportToSFtp(f);
            }
            
            if (conf.eMailSendReport()) {
                sendReport(f);
            }

            l.info("Done!");
        } finally {
            if (cnn != null)
                cnn.close();
        }
    }
    
    private static void importToDatabase() throws Exception {
        Connection cnn = null;

        try {
            l.info("Connectiong to DB...");
            Class.forName(conf.dbDriver());
            if (conf.dbLogin() != null) {
                cnn = java.sql.DriverManager.getConnection(conf.dbConnString(), conf.dbLogin(), conf.dbPwd());
            } else {
                cnn = java.sql.DriverManager.getConnection(conf.dbConnString());
            }

            l.info("Reading input file...");
            File f = new File(new ExpressionParser().parse(conf.filename()));
            importData(f, cnn);
            l.info("Done!");
        } finally {
            if (cnn != null)
                cnn.close();
        }
    }

    private static void exportData(File f, Connection cnn) throws ElitException, IOException, SQLException {
        l.info("Exporting data...");
        ExportLineFormater elf = new ExportLineFormater(conf.csvFormat(), conf.csvFixedSize());
        if (conf.csvDelimiter() != null) {
            elf.setDelimiter(conf.csvDelimiter());
        }
        FileExporter e = new FileExporter(
            cnn,
            conf.sqlIn(),
            elf,
            f,
            conf.csvEncoding(),
            conf.csvUseHeader()
        );
        e.run();
    }
    
    private static void importData(File f, Connection cnn) throws ElitException, IOException, SQLException {
        l.info("Importing data...");
        ImportLineParser ilp = new ImportLineParser(conf.csvFormat(), conf.csvFixedSize());
        if (conf.csvDelimiter() != null) {
            ilp.setDelimiter(conf.csvDelimiter());
        }
        FileImporter i = new FileImporter(
            cnn,
            conf.sqlOutInsert(),
            ilp,
            f,
            conf.csvEncoding(),
            conf.csvUseHeader()
        );
        i.run();
    }

    private static void makeZip(File f) throws ElitException, IOException {
        l.info("Creating zip...");
        File fa = new File(new ExpressionParser().parse(conf.fileArchive()));
        FileZipper fz = new FileZipper(fa);
        fz.addFile(f.toString(), f.getName());
        fz.createZip();
        f.delete();
        f = fa;
    }

    private static void exportToFtp(File f) throws SocketException, IOException {
        l.info("Uploading at FTP...");
        FTPClient client = new FTPClient();
        FtpCommandListener listener = new FtpCommandListener(l);
        client.addProtocolCommandListener(listener);
        FileInputStream fis = null;
        if (conf.ftpPort() == null) {
            client.connect(conf.ftpHost());
        } else {
            client.connect(conf.ftpHost(), Integer.parseInt(conf.ftpPort()));
        }
        client.login(conf.ftpUser(), conf.ftpPwd());
        client.changeWorkingDirectory(conf.ftpDir());
        client.enterLocalPassiveMode();
        client.setFileType(FTPClient.BINARY_FILE_TYPE);
        fis = new FileInputStream(f);
        client.storeFile(f.getName(), fis);
        client.logout();
        fis.close();
        client.disconnect();
    }
    
    private static void exportToFtps(File f) throws SocketException, IOException {
        l.info("Uploading at FTPS...");
        FTPSClient client = new FTPSClient();
        FtpCommandListener listener = new FtpCommandListener(l);
        client.addProtocolCommandListener(listener);
        FileInputStream fis = null;
        if (conf.ftpsPort() == null) {
            client.connect(conf.ftpsHost());
        } else {
            client.connect(conf.ftpsHost(), Integer.parseInt(conf.ftpsPort()));
        }
        client.login(conf.ftpsUser(), conf.ftpsPwd());
        client.changeWorkingDirectory(conf.ftpsDir());
        client.enterLocalPassiveMode();
        client.setFileType(FTPClient.BINARY_FILE_TYPE);
        fis = new FileInputStream(f);
        client.storeFile(f.getName(), fis);
        client.logout();
        fis.close();
        client.disconnect();
    }

    private static void exportToSFtp(File f) throws JSchException, SftpException {
        l.info("Uploading at SFTP...");
        SftpClient client = new SftpClient(
        conf.sftpHost(),
        conf.sftpDir(),
        Integer.valueOf(conf.sftpPort()),
        conf.sftpUser(),
        conf.sftpPwd());
        client.put(f.getAbsolutePath(), f.getName());
    }
    
    private static void sendReport(File f) {
        l.info("Sending report by e-mail...");
        MailSender mail = new MailSender(conf.eMailSmtp(), conf.eMailLogin(), conf.eMailPassword(), conf.eMailSender());
        try {
            File[] a = new File[1];
            a[0] = f;
            mail.sendMail(conf.eMailRecipients(), conf.eMailReportSubject(), conf.eMailReportMessage(), a);
        } catch (EmailException ex) {
            l.log(Level.SEVERE, null, ex);
        }
    }
    
    private static void notifySuccess() {
        if (!conf.eMailNotifyOnSuccess()) {
            l.info("Skipping success notification");
            return;
        }
        MailSender mail = new MailSender(conf.eMailSmtp(), conf.eMailLogin(), conf.eMailPassword(), conf.eMailSender());
        String msg = conf.jobName() + " has been done sucessfully";
        try {
            mail.sendMail(conf.eMailRecipients(), msg, msg);
        } catch (EmailException ex) {
            l.log(Level.SEVERE, null, ex);
        }
        l.info("Success notification has been sent at " + conf.eMailRecipients());
    }
    
    private static void notifyFailure(Exception e) {
        if (!conf.eMailNotifyOnFailure()) {
            l.info("Skipping failure notification");
            return;
        }
        MailSender mail = new MailSender(conf.eMailSmtp(), conf.eMailLogin(), conf.eMailPassword(), conf.eMailSender());
        String msg = conf.jobName() + " has faild";
        try {
            mail.sendMail(conf.eMailRecipients(), msg, msg + "<br><br>" + e.toString());
        } catch (EmailException ex) {
            l.log(Level.SEVERE, null, ex);
        }
        l.info("Failure notification has been sent at " + conf.eMailRecipients());
    }
}
