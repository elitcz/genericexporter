/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

/**
 *
 * @author berk
 */

public class DateType extends DateTimeType {
    
    public DateType() {
        setType(AbstractType.typeDate);
    }
    
    @Override
    public void setValue(IValue value) {
        setValue(value.getDate());
    }
}
