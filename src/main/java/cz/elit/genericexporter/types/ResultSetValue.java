/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author berk
 */
public class ResultSetValue implements IValue {
    
    private ResultSet rs;
    private int index;
    
    public ResultSetValue(ResultSet rs, int index) {
        this.rs = rs;
        this.index = index;
    }
    
    public Date getDateTime() {
        Date result = null;
        try {
            result = rs.getTimestamp(index);
        } catch (SQLException ex) {
            Logger.getLogger(ResultSetValue.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public Date getDate() {
        Date result = null;
        try {
            result = rs.getDate(index);
        } catch (SQLException ex) {
            Logger.getLogger(ResultSetValue.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public double getDouble() {
        double result = 0;
        try {
            result = rs.getDouble(index);
        } catch (SQLException ex) {
            Logger.getLogger(ResultSetValue.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public int getInt() {
        int result = 0;
        try {
            result = rs.getInt(index);
        } catch (SQLException ex) {
            Logger.getLogger(ResultSetValue.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String getString() {
        String result = null;
        try {
            result = rs.getString(index);
        } catch (SQLException ex) {
            Logger.getLogger(ResultSetValue.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
}
