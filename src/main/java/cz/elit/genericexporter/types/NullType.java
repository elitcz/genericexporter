/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

/**
 *
 * @author berk
 */
public class NullType extends AbstractType {
    
    public NullType() {
        setType(AbstractType.typeNull);
    }

    @Override
    public String toString() {
        String result = "null";

        if (getFixedSize() > 0) {
            result = alignLeft(result, getFixedSize());
        }
        
        return result;
    }
    
    @Override
    public void setValue(IValue value) {
    }
    
    @Override
    public void setValue(String value) {
    }

    @Override
    public void clear() {
    }
    
}
