/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 *
 * @author berk
 */
public class IntType extends AbstractType {
    private int value;
    
    public IntType() {
        setType(AbstractType.typeInt);
    }
    
    @Override
    public String toString() {
        String result = String.valueOf(value);
        
        if ((!getFormat().equals(""))) {
            DecimalFormat df = new DecimalFormat(getFormat(), new DecimalFormatSymbols(Locale.US));
            result = df.format(value);
        }

        if (getFixedSize() != 0) {
            result = alignRight(result, getFixedSize());
        }
        
        return result;
    }
    
    @Override
    public void setValue(IValue value) {
        this.value = value.getInt();
    }
    
    @Override
    public void setValue(String value) {
        this.value = Integer.valueOf(value);
    }
    
    @Override
    public void clear() {
        this.value = 0;
    }
    
}
