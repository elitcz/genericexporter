/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

/**
 *
 * @author berk
 */
public abstract class AbstractType {
    
    public static final int typeDateTime = 0;
    public static final int typeDate = 1;
    public static final int typeDouble = 2;
    public static final int typeInt = 3;
    public static final int typeNull = 4;
    public static final int typeString = 5;
    
    private int type;
    private String format = "";
    private int fixedSize = 0;
    
    public void setFormat(String format) {
        this.format = format;
    }
    
    public String getFormat() {
        return format;
    }
    
    public void setFixedSize(int fixedSize) {
        this.fixedSize = fixedSize;
    }
    
    public int getFixedSize() {
        return fixedSize;
    }
    
    @Override
    public abstract String toString();
    public abstract void setValue(IValue value);
    public abstract void setValue(String value);
    public abstract void clear();
    
    protected String alignLeft(String s, int n) {
        String result = "";
        if (s.length() > n) {
            result = s.substring(0, n);
        } else {
            result = s;
            for (int i = 0; i < (n - s.length()); i++) {
                result += " ";
            }
        }
        return result;
    }

    protected String alignRight(String s, int n) {
        String result = "";
        if (s.length() > n) {
            result = s.substring(0, n);
        } else {
            result = s;
            for (int i = 0; i < (n - s.length()); i++) {
                result = " " + result;
            }
        }
        return result;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    protected void setType(int type) {
        this.type = type;
    }
}
