/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 *
 * @author berk
 */
public class DoubleType extends AbstractType {
    private double value;
    
    public DoubleType() {
        setType(AbstractType.typeDouble);
    }

    @Override
    public String toString() {
        String result = String.valueOf(value);
        
        if ((!getFormat().equals(""))) {
            DecimalFormat df = new DecimalFormat(getFormat(), new DecimalFormatSymbols(Locale.US));
            result = df.format(value);
        }
        
        if (getFixedSize() > 0) {
            result = alignRight(result, getFixedSize());
        }
        
        return result;
    }

    @Override
    public void setValue(IValue value) {
        this.value = value.getDouble();
    }
    
    @Override
    public void setValue(String value) {
        this.value = Double.valueOf(value);
    }

    @Override
    public void clear() {
        this.value = 0;
    }
    
}
