/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author berk
 */

public class DateTimeType extends AbstractType {
    private Date value;
    
    public DateTimeType() {
        setType(AbstractType.typeDateTime);
    }
    
    protected void setValue(Date value) {
        this.value = value;
    }

    @Override
    public String toString() {
        String result = "";
        if (value != null) {
            result = String.valueOf(value);

            if ((!getFormat().equals(""))) {
                SimpleDateFormat sdf = new SimpleDateFormat(getFormat());
                result = sdf.format(value);
            }
        } else {
            result = "null";
        }
        
        if (getFixedSize() > 0) {
            result = alignLeft(result, getFixedSize());
        }
        
        return result;
    }

    @Override
    public void setValue(IValue value) {
        this.value = value.getDateTime();
    }
    
    @Override
    public void setValue(String value) {
        SimpleDateFormat sdf = new SimpleDateFormat(getFormat());
        try {
            this.value = sdf.parse(value);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void clear() {
        this.value = null;
    }
    
}
