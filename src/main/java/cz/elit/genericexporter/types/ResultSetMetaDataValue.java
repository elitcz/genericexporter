/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author berk
 */
public class ResultSetMetaDataValue implements IValue {
    private ResultSetMetaData rs;
    private int index;
    
    public ResultSetMetaDataValue(ResultSetMetaData rs, int index) {
        this.rs = rs;
        this.index = index;
    }
    
    public Date getDateTime() {
        return null;
    }

    public Date getDate() {
        return null;
    }

    public double getDouble() {
        return 0;
    }

    public int getInt() {
        return 0;
    }

    public String getString() {
        String result = null;
        try {
            result = rs.getColumnName(index);
        } catch (SQLException ex) {
            Logger.getLogger(ResultSetValue.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
