/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

/**
 *
 * @author berk
 */

public class StringType extends AbstractType {
    private String value;

    public StringType() {
        setType(AbstractType.typeString);
    }
    
    @Override
    public String toString() {
        String result = "";
        if (value != null) {
            result = value;
        } else {
            result = "null";
        }

        if (getFixedSize() > 0) {
            result = alignLeft(result, getFixedSize());
        }
        
        return result;
    }
    
    @Override
    public void setValue(IValue value) {
        this.value = value.getString();
    }
    
    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public void clear() {
        this.value = "";
    }
    
}
