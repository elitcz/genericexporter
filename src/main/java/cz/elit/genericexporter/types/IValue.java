/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter.types;

import java.util.Date;

/**
 *
 * @author berk
 */
public interface IValue {
    
    public Date getDateTime();
    public Date getDate();
    public double getDouble();
    public int getInt();
    public String getString();
    
}
