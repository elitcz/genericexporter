/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter;

import cz.elit.genericexporter.types.AbstractType;
import cz.elit.genericexporter.types.DateTimeType;
import cz.elit.genericexporter.types.DateType;
import cz.elit.genericexporter.types.DoubleType;
import cz.elit.genericexporter.types.IntType;
import cz.elit.genericexporter.types.StringType;
import java.util.ArrayList;

/**
 *
 * @author berk
 */
public class ImportExportLineProcessor {

    protected ArrayList<AbstractType> createRecord(String[] format, String[] fixedSize) {
        ArrayList<AbstractType> result = new ArrayList<AbstractType>();
        
        for (int i = 0; i < format.length; i++) {
            String ss[] = format[i].split("\\(");
            String dataType = ss[0];
            AbstractType type = null;

            if (dataType.equals("string")) {
                type = new StringType();
            }
            if (dataType.equals("int")) {
                type = new IntType();
            }
            if (dataType.equals("double")) {
                type = new DoubleType();
            }
            if (dataType.equals("date")) {
                type = new DateType();
            }
            if (dataType.equals("datetime")) {
                type = new DateTimeType();
            }
            if (type == null) {
                continue;
            }
            if (ss.length > 1) {
                type.setFormat(ss[1].substring(0, ss[1].length() - 1));
            }
            result.add(type);
        }
        
        if (fixedSize != null) {
            for (int i = 0; i < fixedSize.length; i++) {
                result.get(i).setFixedSize(Integer.valueOf(fixedSize[i]));
            }   
        }
        return result;
    }
    
    protected ArrayList<AbstractType> createHeaderRecord(String[] format, String[] fixedSize) {
        ArrayList<AbstractType> result = new ArrayList<AbstractType>();
        
        for (int i = 0; i < format.length; i++) {
            result.add(new StringType());
        }
        
        if (fixedSize != null) {
            for (int i = 0; i < fixedSize.length; i++) {
                result.get(i).setFixedSize(Integer.valueOf(fixedSize[i]));
            }   
        }
        return result;
    }
}
