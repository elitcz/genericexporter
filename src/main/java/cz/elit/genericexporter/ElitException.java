
package cz.elit.genericexporter;

public class ElitException extends Exception {

    public ElitException() {
    }

    public ElitException(String msg) {
        super(msg);
    }
}
