
package cz.elit.genericexporter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpressionParser {

    private String buffer;

    public String parse(String s) throws ElitException {
        if (s == null) {
            return null;
        }

        buffer = s;
        do {} while (parseBuff());
        return buffer;
    }

    private boolean parseBuff() throws ElitException {
        boolean result = false;
        Pattern p = Pattern.compile("\\{(.*?)\\}");
        Matcher m = p.matcher(buffer);
        if (m.find()) {
            result = true;
            String s = m.group(1);
            buffer = buffer.replace("{" + s + "}", translateExpression(s));
        }
        return result;
    }

    private String translateExpression(String exp) throws ElitException {
        String result = exp;
        String fmt = "yyyy-MM-dd";

        if (exp.startsWith("$d")) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());

            exp = exp.substring(2);

            if (exp.startsWith("(")) {
                int i = exp.indexOf(")");
                if (i == -1) {
                    throw new ElitException("Bad date expression");
                }
                fmt = exp.substring(1, i);
                exp = exp.substring(i + 1);
            }

            if (exp.length() > 0) {
                exp = exp.replaceAll("\\s", "");
                exp = exp.replaceAll("\\+", "");
                cal.add(Calendar.DAY_OF_YEAR, Integer.parseInt(exp));
            }
            Date d = cal.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat(fmt);
            result = sdf.format(d);
        }
        if (exp.startsWith("\\t")) {
            result = "\t";
        }
        return result;
    }

}
