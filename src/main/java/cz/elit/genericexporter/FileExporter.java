
package cz.elit.genericexporter;

import cz.elit.genericexporter.types.ResultSetMetaDataValue;
import cz.elit.genericexporter.types.ResultSetValue;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;

public class FileExporter {

    private Connection cnn;
    private String sql;
    private File fout;
    private String encoding;
    private ExportLineFormater exportLineFormater;
    private boolean printHeader;

    public FileExporter(Connection cnn, String sql, ExportLineFormater elf, File fout, String encoding, boolean printHeader) throws ElitException {
        this.cnn = cnn;
        ExpressionParser p = new ExpressionParser();
        this.sql = p.parse(sql);
        this.exportLineFormater = elf;
        this.fout = fout;
        this.encoding = encoding;
        this.printHeader = printHeader;
    }

    public void run() throws IOException, SQLException {
        BufferedWriter w;
        if (encoding != null) {
            w = new BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(fout),encoding));
        } else {
            w = new BufferedWriter(new FileWriter(fout));
        }
        Statement st = cnn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        ResultSetMetaData rsmd = rs.getMetaData();
        int n = rsmd.getColumnCount();
        if (this.printHeader) {
            for (int i = 0; i < n; i++) {
                ResultSetMetaDataValue v = new ResultSetMetaDataValue(rsmd, i + 1);
                exportLineFormater.addHeaderField(v, i);
            }
            w.write(exportLineFormater.getHeaderLine());
            w.newLine();
            exportLineFormater.clear();
        }
        while(rs.next()) {
            for (int i = 0; i < n; i++) {
                ResultSetValue v = new ResultSetValue(rs, i + 1);
                exportLineFormater.addField(v, i);
            }
            w.write(exportLineFormater.getLine());
            w.newLine();
            exportLineFormater.clear();
        }
        rs.close();
        st.close();
        w.close();
    }
}
