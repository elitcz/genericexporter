
package cz.elit.genericexporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileZipper {

    ArrayList<String[]> files = new ArrayList<String[]>();
    File zipFile;

    public FileZipper(File archive) {
        zipFile = archive;
    }

    public void addFile(String sourceFile, String entryFile) {
        String s[] = new String[2];
        s[0] = sourceFile;
        s[1] = entryFile;
        files.add(s);
    }

    public void createZip() throws IOException {

        // Create a buffer for reading the files
        byte[] buf = new byte[1024];
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));
        out.setLevel(9);

        // Compress the files
        for (int i = 0; i < files.size(); i++) {
            FileInputStream in = new FileInputStream(files.get(i)[0]);

            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry(files.get(i)[1]));

            // Transfer bytes from the file to the ZIP file
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            // Complete the entry
            out.closeEntry();
            in.close();
        }

        // Complete the ZIP file
        out.close();
    }
}
