/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.elit.genericexporter;

import java.util.logging.Logger;
import org.apache.commons.net.ProtocolCommandEvent;
import org.apache.commons.net.ProtocolCommandListener;

/**
 *
 * @author berk
 */
public class FtpCommandListener implements ProtocolCommandListener {
    
    private Logger logger;
    
    public FtpCommandListener(Logger logger) {
        this.logger = logger;
    }

    public void protocolCommandSent(ProtocolCommandEvent pce) {
        logger.info(pce.getCommand());
    }

    public void protocolReplyReceived(ProtocolCommandEvent pce) {
        logger.info(pce.getMessage());
    }
    
    
}
