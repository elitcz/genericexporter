# Generic Exporter

Generic Exporter is simple tool for exporting data from database to different targets: 

* File with fixed columns size
* CSV file
* Compress file in zip format
* Send file to FTP or SFTP
* Export data directly into another database

## Usage

Generic exporter supports two kinds of config files:

1. Older style of config file .conf
2. Newer style of config file .properties

### List of all parameters

| Conf file options  | Properties file options | Description  |
| -----------------  | ----------------------- | ------------ |
| **General information** |
| Job Name | job.name | Job name |
| Job Command | job.command | values: exportToFile, exportToDatabase, importFromFile |
| Log File | log.file | Log file |
| $include | include | Include another configuration file |
| **Source database connection** |
| Database Driver | db.driver | Database driver class |
| Connection String | db.connection.string | Connection string |
| Login | db.login | Database login |
| Password | db.pwd | Database password |
| SQL | sql.in | Source SQL select |
| **Destination database connection** |
| Dest. Database Driver | out.db.driver | Destination database driver |
| Dest. Connection String | out.db.connection.string | Destination connection string |
| Dest. Login | out.db.login | Destination database login |
| Dest. Password | out.db.password | Destination database password |
| Dest. SQL_DELETE | sql.out.delete | Destination database command for deleting data |
| Dest. SQL_INSERT | sql.out.insert | Destination database command for inserting data |                            |
| **Destination CSV file** |
| File Name | file.name | Destination file name |
| Archive Name | file.archive | Destination zip archive name |
| Print header | csv.use.header | Option if destination file should include header |
| Encoding | csv.encoding | Destination file encoding |
| Format | csv.cols.format | Destination file columns format |
| Fixed size | csv.fixed.size | Column fixed sizes |
| Delimiter | csv.delimiter | Column delimiter |
| **FTP options** |
| FTP host | ftp.host | FTP server |
| FTP user | ftp.user | FTP user |
| FTP password | ftp.password | FTP password  |
| FTP directory | ftp.directory | FTP directory |
| **FTPS options** |
| FTPS host | ftps.host | FTPS server |
| FTPS user | ftps.user | FTPS user |
| FTPS password | ftps.password | FTPS password  |
| FTPS directory | ftps.directory | FTPS directory |
| **SFTP options** |
| SFTP host | sftp.host | SFTP server |
| SFTP user | sftp.user | SFTP user |
| SFTP password | sftp.password | SFTP password |
| SFTP port | sftp.port | SFTP port |
| **eMail notifications** |
| eMail Notify On Success | email.notifyonsuccess | Enable success notification |
| eMail Notify On Failure | email.notifyonfailure | Enable failure notification |
| eMail Sender | email.sender | eMail sender |
| eMail Recipients | email.recipients | eMail recipients |
| eMail Smtp | email.smtp | eMail server |
| eMail Login | email.login | eMail login |
| eMail Password | email.password | eMail password |
| eMail Send Report | email.sendreport | eMail Send Report |
| eMail Report Subject | email.reportsubject | eMail Report Subject |
| eMail Report Message | email.reportmessage | eMail Report Message |

### Older style of config file .conf

EXAMPLE 1: Export data into file, compress it and send to FTP

```
Job Name: Customer
Job Command: exportToFile

#Log file
Log File: Customer.log

# Source query
SQL:

SELECT [Id]
      ,[Description]
      ,[Price]
      ,[Last Modification Date]
FROM [Item]


#Formating
Print header:	0
Delimiter:	;
#Fixed size:	2,20,2
Format:		string,string,double(##0.00),date(yyyy-MM-dd)

#Driver and connection string
Database Driver:	com.microsoft.sqlserver.jdbc.SQLServerDriver
Connection String:	jdbc:sqlserver://localhost;databaseName=PRODUCT;integratedSecurity=true;

#Export file
File Name:		item.txt.{$d(yyyyMMdd)-1}
#Archive Name:		item_{$d(ddMMyyyy)}.zip

#FTP parameters
FTP host:		ftp.cz
FTP user:		user
FTP password:		pass
FTP directory:		                          
```

EXAMPLE 2: Export data from database to another database

```
Job Name: Customer
Job Command: exportToDatabase

#Log file
Log File: customer.log

# Source query
SQL :
SELECT 
      [Id]
      ,[Name]
FROM [Customer]


# Destioantion delete
Dest. SQL_DELETE:
DELETE FROM [Customer]

# Destioantion insert
Dest. SQL_INSERT:
INSERT INTO [Customer]
           ([No_]
           ,[Name])
     VALUES
           (?,?)


#Source connection:
Database Driver:		com.microsoft.sqlserver.jdbc.SQLServerDriver
Connection String:		jdbc:sqlserver://server1;databaseName=PRODUCTION;integratedSecurity=true;

#Destination connection:
Dest. Database Driver:		com.microsoft.sqlserver.jdbc.SQLServerDriver
Dest. Connection String:	jdbc:sqlserver://server2;databaseName=ESHOP;integratedSecurity=true;

```

EXAMPLE 3: Import data from file to database

```
Job Name:	Import Test
Job Command: importFromFile

#Log file
Log File: import_test.log

# Destioantion insert
Dest. SQL_INSERT:
INSERT INTO IMPORT_TABLE (ID, DESCRIPTION, STATUS, RECORD_DATE, RECORD_DATETIME, QTY) VALUES (?,?,?,?,?,?)

#Formating
Delimiter:		;
#Fixed size:		7,25,10,10,30,30,10,50,10,50
Format:			int,string,int,date(yyyy-MM-dd),datetime(yyyy-MM-dd HH:mm:ss),double
Encoding:		WINDOWS-1250
#Print header:		1

# Driver and connection string
Database Driver:		org.firebirdsql.jdbc.FBDriver
Connection String:		jdbc:firebirdsql:localhost/3050:TEST?lc_ctype=UNICODE_FSS
Login:				login
Password:			pwd

# Import file
File Name:			import.txt
```
